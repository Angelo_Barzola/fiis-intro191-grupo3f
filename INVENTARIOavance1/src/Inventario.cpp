#include <iostream>
#include <string>
#include <windows.h>
#include <conio.h>
#include <stdio.h>
#include <time.h>
#include <cstdlib>
#include <fstream>
#include <iomanip>

#define Color_Red 12
#define Color_Def 15
#define Color_Yellow 14
#define Color_Green 10
#define Delay_us 0
#define Cursor 16
#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77
#define KEY_ENTER 13
#define BACKSPACE 8
#define KEY_ESC 27
#define Max_Chars 12
#define USER "AngeloUNI"
#define PASS "barzolaponce"

using namespace std;
char KEY_PAD;

void login();
void titulo(void);
void RegistroUsuario();
void opciones();
void microprocesador1();
	void microamd();
	void microintel();
void MemoriaRAM2();
	void korsair();
	void kingston();
	void gskill();
void PlacaMadre3();
	void placaasus();
	void placagyga();
void DiscoDuro4();
	void discossd();
	void discosata();
	void discosas();
void TarjetaVideo5();
	void tarjeasnvidia();
	void tarjeamd();
void salir();
void gotoxy(int x,int y);



int main()
{
RegistroUsuario();



system("pause>0");
return 0;
}

void titulo(void){
	int X=28,Y=0;
	system("color 0b");
    char Line1[] = {" CCC   OO   MM MM  PPP   U  U  ssss  TTTTTT   OO   RRRR  EEEE  "};
    char Line2[] = {" C    O  0  MM MM  P  P  U  U  Ssss    TT    O  O  RRRR  Eeee  "};
    char Line3[] = {" C    0  0  M M M  PPP   U  U      S   TT    O  O  R R   E     "};
    char Line4[] = {" CCC   00   M   M  P      UU   ssss    TT     OO   R  R  EEEE  "};


    gotoxy (X,Y+2);
    for (unsigned int i= 0; i < sizeof(Line1); i++)
    {
        cout << Line1[i];
        Sleep(Delay_us);
    }
    gotoxy (X,Y+3);
    for (unsigned int i= 0; i < sizeof(Line2); i++)
    {
        cout << Line2[i];
        Sleep(Delay_us);
    }
    gotoxy (X,Y+4);
    for (unsigned int i= 0; i < sizeof(Line3); i++)
    {
        cout << Line3[i];
        Sleep(Delay_us);
    }
    gotoxy (X,Y+5);
    for (unsigned int i= 0; i < sizeof(Line4); i++)
    {
        cout << Line4[i];
        Sleep(Delay_us);
    }
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Yellow);
}

void RegistroUsuario(){
int Cursor_Pos = 12;

titulo();
gotoxy(46,11);
cout << " ------------- ";
gotoxy(46,12);
cout << "|   INGRESAR  |";
gotoxy(46,13);
cout << "|             |";
gotoxy(46,14);
cout << "|   SALIR     |";
gotoxy(46,15);
cout << " ------------- ";

 gotoxy(47,Cursor_Pos);
 SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Yellow);
 cout << (char)Cursor;
 SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
 gotoxy(1,1);

 do{
     if (kbhit())
     {
         KEY_PAD = getch();

         switch (KEY_PAD)
         {
             case KEY_UP:
             {
                 if (Cursor_Pos >= 14) Cursor_Pos = Cursor_Pos - 2;
             }
             break;

             case KEY_DOWN:
             {
                 if (Cursor_Pos <= 12) Cursor_Pos = Cursor_Pos + 2;
             }
             break;

             case KEY_ENTER:
             {
                 switch (Cursor_Pos)
                 {
                     case 12: return login(); break;
                     case 14: return salir(); break;

                 }
             }
             break;

             case KEY_ESC: return salir(); break;

         }

         for (int i = 0; i<4 ; i+=2)
         {
             gotoxy(47,12+i);
             cout << (char) 0;
         }

         gotoxy(47,Cursor_Pos);
         SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Yellow);
         cout << (char)Cursor;
         SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
         gotoxy(1,1);
     }
 }while (KEY_PAD!=27);

 return salir();

}

void login(){
    string usuario, password;

    int contador = 0;
    bool ingresa = false;

    do {
    	system("cls");
    	titulo();
    	gotoxy(3,7);
        cout << "LOGIN DE USUARIO" << endl;
        gotoxy(5,10);
        cout << "Usuario: ";
        getline(cin, usuario);

        char caracter;
        gotoxy(5,12);
        cout << "Password: ";
        caracter = getch();

        password = "";

        while (caracter != KEY_ENTER) {

        if (caracter != BACKSPACE) {
            password.push_back(caracter);
            cout << "*";

        } else {
            if (password.length() > 0) {
                cout << "\b \b";
                password = password.substr(0, password.length() - 1);
            }
        }

        caracter = getch();
        }


        if (usuario == USER && password == PASS) {
            ingresa = true;
        } else {
            cout << "\n\n\tEl usuario y/o password son incorrectos" << endl;
            contador++;
            if(3-contador!=0){
            cout << "\n\n\tTe quedan "<<3-contador<<" intentos"<<endl<<endl;}
            else{cout << "\n\n\tNo se pudo acceder al sistema" << endl;}
            cin.get();

        }

    } while (ingresa == false && contador < 3);

    if (ingresa == false) {
        return salir();
    } else {
        cout << "\n\ntBienvenido al sistema" << endl;
        return opciones();
    }

    cin.get();
}

void opciones(){

	int Cursor_Pos = 10;
	system("cls");

	titulo();


    gotoxy(3,7);
    cout << "Hardware interno: ";

    gotoxy(5,10);
    cout << "Microprocesador";

    gotoxy(5,12);
    cout << "Memoria RAM";

    gotoxy(5,14);
    cout << "Placa Madre";

    gotoxy(5,16);
    cout << "Disco Duro";

    gotoxy(5,18);
    cout << "Tarjeta de Video";
    gotoxy(5,20);
    cout << "Salir";


    gotoxy(3,Cursor_Pos);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
    cout << (char)Cursor;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
    gotoxy(1,1);

    do{
        if (kbhit())
        {
            KEY_PAD = getch();

            switch (KEY_PAD)
            {
                case KEY_UP:
                {
                    if (Cursor_Pos >= 12) Cursor_Pos = Cursor_Pos - 2;
                }
                break;

                case KEY_DOWN:
                {
                    if (Cursor_Pos <= 18) Cursor_Pos = Cursor_Pos + 2;
                }
                break;

                case KEY_ENTER:
                {
                    switch (Cursor_Pos)
                    {
                        case 10: return microprocesador1(); break;
                        case 12: return MemoriaRAM2(); break;
                        case 14: return PlacaMadre3(); break;
                        case 16: return DiscoDuro4(); break;
                        case 18: return TarjetaVideo5(); break;
                        case 20: return salir(); break;

                    }
                }
                break;

                case KEY_ESC: return salir(); break;

            }

            for (int i = 0; i<12 ; i+=2)
            {
                gotoxy(3,10+i);
                cout << (char) 0;
            }

            gotoxy(3,Cursor_Pos);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
            cout << (char)Cursor;
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
            gotoxy(1,1);
        }
    }while (KEY_PAD!=27);

    return salir();

}

void salir(){
	exit(1);
}

void microprocesador1(){
	system("color 0e");
	int Cursor_Pos = 10;
	system("cls");
	titulo();


    gotoxy(3,7);
    cout << "MICROPROCESADORES";

    gotoxy(5,10);
    cout << "Procesadores INTEL";

    gotoxy(5,12);
    cout << "Procesadores AMD";

    gotoxy(5,14);
    cout << "Regresar";

    gotoxy(5,16);
    cout << "Salir";

    gotoxy(3,Cursor_Pos);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
    cout << (char)Cursor;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
    gotoxy(1,30);

    do{
        if (kbhit())
        {
            KEY_PAD = getch();

            switch (KEY_PAD)
            {
                case KEY_UP:
                {
                    if (Cursor_Pos >= 12) Cursor_Pos = Cursor_Pos - 2;
                }
                break;

                case KEY_DOWN:
                {
                    if (Cursor_Pos <= 14) Cursor_Pos = Cursor_Pos + 2;
                }
                break;

                case KEY_ENTER:
                {
                    switch (Cursor_Pos)
                    {
                        case 10: return microintel(); break;
                        case 12: return microamd();; break;
                        case 14: return opciones(); break;
                        case 16: return salir(); break;
                    }
                }
                break;

                case KEY_ESC: return salir(); break;

            }

            for (int i = 0; i<8 ; i+=2)
            {
                gotoxy(3,10+i);
                cout << (char) 0;
            }

            gotoxy(3,Cursor_Pos);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
            cout << (char)Cursor;
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
            gotoxy(1,30);
        }
    }while (KEY_PAD!=27);

    return salir();

}

	void microamd(){
		int Cursor_Pos=38;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   MICROPROCESADOR AMD ";

	    gotoxy(5,10);
	    cout << "PROCESADOR AMD A10-9700";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"3.50GHZ, 2MB, L2 10 CORES, 65W, CAJA.";
	    gotoxy(7,13);cout<<"Stock: 20";
	    gotoxy(7,14);cout<<"Precio: S/. 295,00";

	    gotoxy(5,17);
	    cout << "PROCESADOR AMD A6-9400";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"3.70GHZ, 1MB CACHE";
	    gotoxy(7,20);cout<<"Stock: 2";
	    gotoxy(7,21);cout<<"Precio: S/. 135,00";

	    gotoxy(5,24);
	    cout << "PROCESADOR AMD A6-9500";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"3.50GHZ, 1MB L2, 8 CORES, AM4, 65W, CAJA.";
	    gotoxy(7,27);cout<<"Stock: 18";
	    gotoxy(7,28);cout<<"Precio: S/. 160,00";

	    gotoxy(5,31);
	    cout << "PROCESADOR AMD A8-7680";
	    gotoxy(7,32);cout<<"Caracteristicas:";
	    gotoxy(7,33);cout<<"3.80GHZ, 2MB CACHE, 4 CORE, FM2+, 65W.";
	    gotoxy(7,34);cout<<"Stock: 15";
	    gotoxy(7,35);cout<<"Precio: S/. 210,00";

	    gotoxy(53,10);
	    cout << "PROCESADOR AMD ATHLON X4 950";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"3.50GHZ 2MB L2, 4 CORES, AM4, 28NM, 45/65W, CAJA.";
	    gotoxy(55,13);cout<<"Stock: 8";
	    gotoxy(55,14);cout<<"Precio: S/. 220,00";

	    gotoxy(53,17);
	    cout << "PROCESADOR AMD RYZEN 3 2200G";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"2200G, 3.50GHZ, 4MB, L3, 4 CORE, AM4, 14NM, 65 W.";
	    gotoxy(55,20);cout<<"Stock: 2";
	    gotoxy(55,21);cout<<"Precio: S/. 405,00";

	    gotoxy(53,24);
	    cout << "PROCESADOR AMD RYZEN 5 2400G";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"3.60GHZ, 4MB L3, 4 CORE, AM4, 14NM, 65W.";
	    gotoxy(55,27);cout<<"Stock: 11";
	    gotoxy(55,28);cout<<"Precio: S/. 610,00";

	    gotoxy(53,31);
	    cout << "PROCESADOR AMD RYZEN 5 2600X";
	    gotoxy(55,32);cout<<"Caracteristicas:";
	    gotoxy(55,33);cout<<"3.60GHZ, 16MB L3, 6 CORE, AM4, 12NM, 95W.";
	    gotoxy(55,34);cout<<"Stock: 13";
	    gotoxy(55,35);cout<<"Precio: S/. 1020,00";


	    gotoxy(5,38);
	    cout << "Regresar";

	    gotoxy(5,40);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 40) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 38) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 38: return microprocesador1(); break;
	                        case 40: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,38+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();


	}

	void microintel(){
		int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   MICROPROCESADOR INTEL ";

	    gotoxy(5,10);
	    cout << "PROCESADOR Intel Core i3-9100F";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"3.60 GHz,6 MB, Cache, LGA1151";
	    gotoxy(7,13);cout<<"Stock: 20";
	    gotoxy(7,14);cout<<"Precio: S/ 485.75";

	    gotoxy(5,17);
	    cout << "PROCESADOR intel core i5-7400";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"3.00 GHZ, 6 MB, CACHÉ L3";
	    gotoxy(7,20);cout<<"Stock: 7";
	    gotoxy(7,21);cout<<"Precio: S/. 1300,00";


	    gotoxy(5,24);
	    cout << "PROCESADOR intel core i7-7700";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"3.60 GHZ, 8 MB, CACHE L3";
	    gotoxy(7,27);cout<<"Stock: 9";
	    gotoxy(7,28);cout<<"Precio: S/. 1400,00";


	    gotoxy(53,10);
	    cout << "PROCESADOR Intel Core i9-9900K";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"3.60 GHz, 16MB LGA1151 8C/16T";
	    gotoxy(55,13);cout<<"Stock: 8";
	    gotoxy(55,14);cout<<"Precio: S/ 2,345.00";

	    gotoxy(53,17);
	    cout << "PROCESADOR Intel Dual Core G5400 ";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"3.7GHz, 4MB LGA1151";
	    gotoxy(55,20);cout<<"Stock: 19";
	    gotoxy(55,21);cout<<"Precio: S/ 308.20";

	    gotoxy(53,24);
	    cout << "PROCESADOR INTEL CELERON G3930";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"2.90GHZ, 2MB L3, LGA1151, 51W, 14NM.";
	    gotoxy(55,27);cout<<"Stock: 2";
	    gotoxy(55,28);cout<<"Precio: S/. 200,00";



	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return microprocesador1(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();



	}


void MemoriaRAM2(){
	system("color 0e");
	int Cursor_Pos = 10;
	system("cls");
	titulo();
    gotoxy(3,7);
    cout << "MEMORIA RAM";

    gotoxy(5,10);
    cout << "CORSAIR";

    gotoxy(5,12);
    cout << "KINGSTON";

    gotoxy(5,14);
    cout << "G SKILL";

    gotoxy(5,16);
    cout << "Regresar";

    gotoxy(5,18);
    cout << "Salir";

    gotoxy(3,Cursor_Pos);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
    cout << (char)Cursor;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
    gotoxy(1,30);

    do{
        if (kbhit())
        {
            KEY_PAD = getch();

            switch (KEY_PAD)
            {
                case KEY_UP:
                {
                    if (Cursor_Pos >= 12) Cursor_Pos = Cursor_Pos - 2;
                }
                break;

                case KEY_DOWN:
                {
                    if (Cursor_Pos <= 16) Cursor_Pos = Cursor_Pos + 2;
                }
                break;

                case KEY_ENTER:
                {
                    switch (Cursor_Pos)
                    {
                        case 10: return korsair(); break;
                        case 12: return kingston(); break;
                        case 14: return gskill(); break;
                        case 16: return opciones(); break;
                        case 18: return salir(); break;

                    }
                }
                break;

                case KEY_ESC: return salir(); break;

            }

            for (int i = 0; i<10 ; i+=2)
            {
                gotoxy(3,10+i);
                cout << (char) 0;
            }

            gotoxy(3,Cursor_Pos);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
            cout << (char)Cursor;
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
            gotoxy(1,30);
        }
    }while (KEY_PAD!=27);

    return salir();

}


	void korsair(){
	    int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   MEMORIA RAM CORSAIR ";

	    gotoxy(5,10);
	    cout << "Memoria RAM CORSAIR DOMINATOR PLATINIUM C10 ";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"3600 MHz, 128GB(8x16GB) DDR4";
	    gotoxy(7,13);cout<<"Stock: 3";
	    gotoxy(7,14);cout<<"Precio: S/ 4600.00";

	    gotoxy(5,17);
	    cout << "Memoria RAM CORSAIR VENGANCE LPX C16";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"3600 MHz, 32GB(2x16GB) DDR4";
	    gotoxy(7,20);cout<<"Stock: 3";
	    gotoxy(7,21);cout<<"Precio: S/ 526.00";

	    gotoxy(5,24);
	    cout << "Memoria RAM CORSAIR DOMINATOR PLATINIUM C15";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"2666 MHz, 128GB(8x16GB) DDR4";
	    gotoxy(7,27);cout<<"Stock: 3";
	    gotoxy(7,28);cout<<"Precio: S/ 6580.00";

	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return MemoriaRAM2(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();



	}

	void kingston(){
	    int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   KINGSTON ";

	    gotoxy(5,10);
	    cout << "Memoria RAM KINGSTON HyperX Fury ";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<" 2666 MHz, 16GB(2x8GB) DDR4";
	    gotoxy(7,13);cout<<"Stock: 5";
	    gotoxy(7,14);cout<<"Precio: S/ 365.00";

	    gotoxy(5,17);
	    cout << "Memoria RAM KINGSTON PREDATOR";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"2400 MHz, 16GB(2x8GB) DDR4";
	    gotoxy(7,20);cout<<"Stock: 3";
	    gotoxy(7,21);cout<<"Precio: S/ 625.00";

	    gotoxy(5,24);
	    cout << "Memoria RAM KINGSTON FURY";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"1600 MHz, 8GB(2x4GB) DDR3L";
	    gotoxy(7,27);cout<<"Stock: 3";
	    gotoxy(7,28);cout<<"Precio: S/ 155.00";

	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return MemoriaRAM2(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();




	}

	void gskill(){
	    int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   G SKILL ";

	    gotoxy(5,10);
	    cout << "Memoria RAM G.Skill RIPJAWS  ";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"1600 MHz, 8GB(2x4GB) DDR3";
	    gotoxy(7,13);cout<<"Stock: 3";
	    gotoxy(7,14);cout<<"Precio: S/ 190.70";

	    gotoxy(5,17);
	    cout << "Memoria RAM G.Skill AEGIS";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"1333 MHz, 8GB(2x4GB) DDR3";
	    gotoxy(7,20);cout<<"Stock: 3";
	    gotoxy(7,21);cout<<"Precio: S/ 190.00";

	    gotoxy(5,24);
	    cout << "Memoria RAM G.Skill SO-DIMM";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"1066 MHz, 8GB(2x4GB) DDR3";
	    gotoxy(7,27);cout<<"Stock: 4";
	    gotoxy(7,28);cout<<"Precio: S/ 185.00";

	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return MemoriaRAM2(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();



	}


void PlacaMadre3(){
	system("color 0e");
	int Cursor_Pos = 10;
	system("cls");
	titulo();

    gotoxy(3,7);
    cout << "PLACAS MADRES ";

    gotoxy(5,10);
    cout << "Placa Madre ASUS";

    gotoxy(5,12);
    cout << "Placa  Madre GYGABYTE";

    gotoxy(5,14);
    cout << "Regresar";

    gotoxy(5,16);
    cout << "Salir";

    gotoxy(3,Cursor_Pos);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
    cout << (char)Cursor;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
    gotoxy(1,30);

    do{
        if (kbhit())
        {
            KEY_PAD = getch();

            switch (KEY_PAD)
            {
                case KEY_UP:
                {
                    if (Cursor_Pos >= 12) Cursor_Pos = Cursor_Pos - 2;
                }
                break;

                case KEY_DOWN:
                {
                    if (Cursor_Pos <= 14) Cursor_Pos = Cursor_Pos + 2;
                }
                break;

                case KEY_ENTER:
                {
                    switch (Cursor_Pos)
                    {
                        case 10: return placaasus(); break;
                        case 12: return placagyga(); break;
                        case 14: return opciones(); break;
                        case 16: return salir(); break;
                    }
                }
                break;

                case KEY_ESC: return salir(); break;

            }

            for (int i = 0; i<8 ; i+=2)
            {
                gotoxy(3,10+i);
                cout << (char) 0;
            }

            gotoxy(3,Cursor_Pos);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
            cout << (char)Cursor;
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
            gotoxy(1,30);
        }
    }while (KEY_PAD!=27);

    return salir();



}


	void placaasus(){
	      int Cursor_Pos=38;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   PLACA MADRE ASUS ";

	    gotoxy(5,10);
	    cout << "PLACA ASUS H170M-E D3 ";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"SN/VD/NW LGA1151 DDR3 USB 3.0";
	    gotoxy(7,13);cout<<"Stock: 7";
	    gotoxy(7,14);cout<<"Precio: S/ 385.25";

	    gotoxy(5,17);
	    cout << "PROCESADOR AMD A6-9400";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"3.70GHZ, 1MB CACHE";
	    gotoxy(7,20);cout<<"Stock: 2";
	    gotoxy(7,21);cout<<"Precio: S/. 135,00";

	    gotoxy(5,24);
	    cout << "PLACA ASUS PRIME B360M-A DDR4";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"USB 3.1 VD/SN/NW LGA1151";
	    gotoxy(7,27);cout<<"Stock:25";
	    gotoxy(7,28);cout<<"Precio: S/ 345.05";

	    gotoxy(5,31);
	    cout << "PLACA ASUS PRIME H310M-ER2.0";
	    gotoxy(7,32);cout<<"Caracteristicas:";
	    gotoxy(7,33);cout<<"LGA1151 DDR4 USB 3.0 SN/VD/NW.";
	    gotoxy(7,34);cout<<"Stock: 20";
	    gotoxy(7,35);cout<<"Precio: S/ 237.85";

	    gotoxy(53,10);
	    cout << "PLACA ASUS PRIME Z390-A DDR4";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"USB 3.1 VD/SN/NW LGA1151";
	    gotoxy(55,13);cout<<"Stock: 6";
	    gotoxy(55,14);cout<<"Precio: S/ 830.80";

	    gotoxy(53,17);
	    cout << "PLACA ASUS ROG STRIX Intel Z390-E";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"2200G, 3.50GHZ, 4MB, L3, 4 CORE, AM4, 14NM, 65 W.";
	    gotoxy(55,20);cout<<"Stock: 2";
	    gotoxy(55,21);cout<<"Precio: S/. 405,00";

	    gotoxy(53,24);
	    cout << "PLACA ASUS Z390 ROG MAXIMUS XI HERO (wi-fi)";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"Gaming LGA1151 nvidia/SLI/ Aura Sync";
	    gotoxy(55,27);cout<<"Stock: 7";
	    gotoxy(55,28);cout<<"Precio: S/ 1,072.00";

	    gotoxy(53,31);
	    cout << "PLACA ASUS Z390 ROG MAXIMUS XI EXTREME";
	    gotoxy(55,32);cout<<"Caracteristicas:";
	    gotoxy(55,33);cout<<"DDR4 USB 3.1 VD/SN/NW LGA1151";
	    gotoxy(55,34);cout<<"Stock: 7";
	    gotoxy(55,35);cout<<"Precio: S/ 2,127.25";



	    gotoxy(5,38);
	    cout << "Regresar";

	    gotoxy(5,40);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 40) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 38) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 38: return PlacaMadre3(); break;
	                        case 40: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,38+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();
	}

	void placagyga(){
	    int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   PLACA MADRE GYGABYTE ";

	    gotoxy(5,10);
	    cout << "PLACA GIGABYTE GA-H110M-H ";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"DDR4 USB 3.0, SN/VD/NW LGA1151";
	    gotoxy(7,13);cout<<"Stock: 7";
	    gotoxy(7,14);cout<<"Precio: S/ 207.70";

	    gotoxy(5,17);
	    cout << "PLACA GIGABYTE H310M-A LGA1151";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"DDR4 USB 3.1 SN/VD/NW/ M.2";
	    gotoxy(7,20);cout<<"Stock: 9";
	    gotoxy(7,21);cout<<"Precio: S/ 241.20";

	    gotoxy(5,24);
	    cout << "PLACA GIGABYTE H310M-H";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"USB 3.1 VD/SN/NW LGA1151";
	    gotoxy(7,27);cout<<"Stock:13";
	    gotoxy(7,28);cout<<"Precio: S/ 237.85";

	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return PlacaMadre3(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();


	}


void DiscoDuro4(){

	system("color 0e");
	int Cursor_Pos = 10;
	system("cls");
	titulo();

    gotoxy(3,7);
    cout << "DISCO DUROS";

    gotoxy(5,10);
    cout << "Discos duros SDD";

    gotoxy(5,12);
    cout << "Discos duros SATA III";

    gotoxy(5,14);
    cout << "Discos duros SAS o SCSI";

    gotoxy(5,16);
    cout << "Regresar";

    gotoxy(5,18);
    cout << "Salir";

    gotoxy(3,Cursor_Pos);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
    cout << (char)Cursor;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
    gotoxy(1,1);

    do{
        if (kbhit())
        {
            KEY_PAD = getch();

            switch (KEY_PAD)
            {
                case KEY_UP:
                {
                    if (Cursor_Pos >= 12) Cursor_Pos = Cursor_Pos - 2;
                }
                break;

                case KEY_DOWN:
                {
                    if (Cursor_Pos <= 16) Cursor_Pos = Cursor_Pos + 2;
                }
                break;

                case KEY_ENTER:
                {
                    switch (Cursor_Pos)
                    {
                        case 10: return discossd(); break;
                        case 12: return ; break;
                        case 14: return ; break;
                        case 16: return opciones(); break;
                        case 18: return salir(); break;
                    }
                }
                break;

                case KEY_ESC: return salir(); break;

            }

            for (int i = 0; i<10 ; i+=2)
            {
                gotoxy(3,10+i);
                cout << (char) 0;
            }

            gotoxy(3,Cursor_Pos);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
            cout << (char)Cursor;
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
            gotoxy(1,1);
        }
    }while (KEY_PAD!=27);

    return salir();



}

	void discossd(){
		int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   DISCO DURO SSD ";

	    gotoxy(5,10);
	    cout << "DISCO DURO INTEL SSDOPTANE";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"32GB, M.2 (22x80mm), 1350 Mbps, 290 Mbps";
	    gotoxy(7,13);cout<<"Stock: 20";
	    gotoxy(7,14);cout<<"Precio:  S/266.07";

	    gotoxy(5,17);
	    cout << "INTEL OPTANE SSD SMEM ";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"32GB, M.2 (22x80mm), 1350 Mbps, 290 Mbps";
	    gotoxy(7,20);cout<<"Stock: 7";
	    gotoxy(7,21);cout<<"Precio:  S/265.04";


	    gotoxy(5,24);
	    cout << "Intel Optane Memory Series";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"16GB, M.2 (22x80mm), 900 Mbps, 145 Mbps";
	    gotoxy(7,27);cout<<"Stock: 9";
	    gotoxy(7,28);cout<<"Precio:  S/169.04";


	    gotoxy(53,10);
	    cout << "SSD INTEL 670P ";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"512GB, 1000 MB/s, 1500 MB/s";
	    gotoxy(55,13);cout<<"Stock: 8";
	    gotoxy(55,14);cout<<"Precio:  S/499.02";

	    gotoxy(53,17);
	    cout << "SSD HYPERX FURY RGB 960GB ";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"SATA 6.0 Gbps, 2.5, Marvell 88SS1074";
	    gotoxy(55,20);cout<<"Stock: 19";
	    gotoxy(55,21);cout<<"Precio:  S/107.27";

	    gotoxy(53,24);
	    cout << "SSD KINGTON 120GB A400";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"SATA 6Gb/s, NAND Flash Memory , 2Ch";
	    gotoxy(55,27);cout<<"Stock: 2";
	    gotoxy(55,28);cout<<"Precio: S/. 200,00";



	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return DiscoDuro4(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();

	}

	void discosata(){
		int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   DISCO DURO SATA ";

	    gotoxy(5,10);
	    cout << "Disco duro  Surveillance";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"10TB,SATA 6.0 Gbps, Cache, 3.5";
	    gotoxy(7,13);cout<<"Stock: 20";
	    gotoxy(7,14);cout<<"Precio:  S/1,252.82";

	    gotoxy(5,17);
	    cout << "Disco duro Seagate SkyHaw";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"1TB, SATA 6.0 Gbps, 5900 RPM, 64MB Cache";
	    gotoxy(7,20);cout<<"Stock: 7";
	    gotoxy(7,21);cout<<"Precio:  S/200.48";


	    gotoxy(5,24);
	    cout << "Disco duro Seagate Barracuda";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"1TB, SATA 6.0 Gbps, 7200 RPM, 64MB Cache";
	    gotoxy(7,27);cout<<"Stock: 9";
	    gotoxy(7,28);cout<<"Precio:  S/182.11";


	    gotoxy(53,10);
	    cout << "Disco duro Toshiba HDKPC03A0A02";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"1TB, SATA 6.0 Gb/s, 7200 RPM, 3.5";
	    gotoxy(55,13);cout<<"Stock: 8";
	    gotoxy(55,14);cout<<"Precio:  S/185.94";

	    gotoxy(53,17);
	    cout << "Disco duro Toshiba HDKPC03A0A02 ";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"1TB\nSATA 6.0 Gb/s, 7200 RPM, 3.5";
	    gotoxy(55,20);cout<<"Stock: 19";
	    gotoxy(55,21);cout<<"Precio:  S/182.78";

	    gotoxy(53,24);
	    cout << "Disco duro Toshiba P300 ";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"2TB, SATA 6.0 Gbps, 7200 RPM, 3.5";
	    gotoxy(55,27);cout<<"Stock: 2";
	    gotoxy(55,28);cout<<"Precio:  S/280.81";



	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return DiscoDuro4(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();

	}

	void discosas(){
		int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   DISCO SAS ";

	    gotoxy(5,10);
	    cout << "Disco duro Dell 400-ALUQ";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"1TB, SAS 12.0 Gb/s, 7500 RPM, 2.5";
	    gotoxy(7,13);cout<<"Stock: 20";
	    gotoxy(7,14);cout<<"Precio:  S/676.91";

	    gotoxy(5,17);
	    cout << "Disco duro Dell 400-ATJG";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"1TB, SAS 6.0 Gb/s, 7200 RPM, 2.5";
	    gotoxy(7,20);cout<<"Stock: 7";
	    gotoxy(7,21);cout<<"Precio:  S/1,636.69";


	    gotoxy(5,24);
	    cout << "Disco duro Dell 400-APZTV";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"1 TB, SAS 6 Gb/s, 7200 RPM, 3.5, 512";
	    gotoxy(7,27);cout<<"Stock: 9";
	    gotoxy(7,28);cout<<"Precio:  S/503.01";


	    gotoxy(53,10);
	    cout << "Disco duro HP 655710-B21";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"1TBM, SAS 6.0, 7200 RPM, 2.5";
	    gotoxy(55,13);cout<<"Stock: 8";
	    gotoxy(55,14);cout<<"Precio:  S/1,683.09";

	    gotoxy(53,17);
	    cout << "Disco duro HP 759208-B21 ";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"300GB, SAS, 15000 RPM, 2.5";
	    gotoxy(55,20);cout<<"Stock: 19";
	    gotoxy(55,21);cout<<"Precio:  S/1,497.48";

	    gotoxy(53,24);
	    cout << "Disco duro HP 737261-B21";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"300GB, SAS 12G, 15000 RPM ,3.5";
	    gotoxy(55,27);cout<<"Stock: 2";
	    gotoxy(55,28);cout<<"Precio:  S/1,316.10";



	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return DiscoDuro4(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();

	}


void TarjetaVideo5(){
	system("color 0e");
	int Cursor_Pos = 10;
	system("cls");
	titulo();

    gotoxy(3,7);
    cout << "TARJETA DE VIDEO ";

    gotoxy(5,10);
    cout << "Tarjeta de video Nvidia";

    gotoxy(5,12);
    cout << "Tarjeta de video AMD";

    gotoxy(5,14);
    cout << "Regresar";

    gotoxy(5,16);
    cout << "Salir";

    gotoxy(3,Cursor_Pos);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
    cout << (char)Cursor;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
    gotoxy(1,30);

    do{
        if (kbhit())
        {
            KEY_PAD = getch();

            switch (KEY_PAD)
            {
                case KEY_UP:
                {
                    if (Cursor_Pos >= 12) Cursor_Pos = Cursor_Pos - 2;
                }
                break;

                case KEY_DOWN:
                {
                    if (Cursor_Pos <= 14) Cursor_Pos = Cursor_Pos + 2;
                }
                break;

                case KEY_ENTER:
                {
                    switch (Cursor_Pos)
                    {
                        case 10: return tarjeasnvidia(); break;
                        case 12: return tarjeamd(); break;
                        case 14: return opciones(); break;
                        case 16: return salir(); break;
                    }
                }
                break;

                case KEY_ESC: return salir(); break;

            }

            for (int i = 0; i<8 ; i+=2)
            {
                gotoxy(3,10+i);
                cout << (char) 0;
            }

            gotoxy(3,Cursor_Pos);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
            cout << (char)Cursor;
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
            gotoxy(1,30);
        }
    }while (KEY_PAD!=27);

    return salir();

}

	void tarjeasnvidia(){
	     int Cursor_Pos=31;
		system("color 0e");

		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   TARJETA DE VIDEO NVIDIA ";

	    gotoxy(5,10);
	    cout << "Tarjeta grafica GTX 1050";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"Nucleos CUDA: 640, 7 Gbps, 2GB DDR5";
	    gotoxy(7,13);cout<<"Stock: 3";
	    gotoxy(7,14);cout<<"Precio: S/ 720.00";

	    gotoxy(5,17);
	    cout << "Tarjeta grafica GTX 1050 TI";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"Nucleos CUDA: 640 7 Gbps, 8GB DDR5";
	    gotoxy(7,20);cout<<"Stock: 3";
	    gotoxy(7,21);cout<<"Precio: S/. 700,00";


	    gotoxy(5,24);
	    cout << "Tarjeta grafica RTX 1660 TI";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"Nucleos CUDA: 1540 12 Gbps, 4GB DDR5";
	    gotoxy(7,27);cout<<"Stock: 9";
	    gotoxy(7,28);cout<<"Precio: S/. 2400,00";


	    gotoxy(53,10);
	    cout << "Tarjeta grafica GTX 1080";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"Nucleos CUDA: 6240 8 Gbps, 4GB DDR5";
	    gotoxy(55,13);cout<<"Stock: 8";
	    gotoxy(55,14);cout<<"Precio: S/ 2,345.00";

	    gotoxy(53,17);
	    cout << "Tarjeta grafica GTX 1080 TI ";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"Nucleos CUDA: 850 7 Gbps, 6GB DDR5";
	    gotoxy(55,20);cout<<"Stock: 4";
	    gotoxy(55,21);cout<<"Precio: S/ 380.20";

	    gotoxy(53,24);
	    cout << "Tarjeta grafica RTX 2070";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"Nucleos CUDA: 1640 4 Gbps, 8GB DDR5";
	    gotoxy(55,27);cout<<"Stock: 7";
	    gotoxy(55,28);cout<<"Precio: S/. 250,00";


	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return TarjetaVideo5(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();




	}

	void tarjeamd(){
	     int Cursor_Pos=31;
		system("color 0e");
		system("cls");
		titulo();
	    gotoxy(3,7);
	    cout << "\n   TARJETA DE VIDEO AMD ";

	    gotoxy(5,10);
	    cout << "Tarjeta grafica RX 480";
	    gotoxy(7,11);cout<<"Caracteristicas:";
	    gotoxy(7,12);cout<<"transmision1054, 6 Gbps, 4GB GDDR5";
	    gotoxy(7,13);cout<<"Stock: 10";
	    gotoxy(7,14);cout<<"Precio: S/ 1070.00";

	    gotoxy(5,17);
	    cout << "Tarjeta grafica RX 470";
	    gotoxy(7,18);cout<<"Caracteristicas:";
	    gotoxy(7,19);cout<<"transmision224, 4 Gbps, 4GB GDDR53";
	    gotoxy(7,20);cout<<"Stock: 5";
	    gotoxy(7,21);cout<<"Precio: S/. 1300,00";


	    gotoxy(5,24);
	    cout << "Tarjeta grafica RX 460";
	    gotoxy(7,25);cout<<"Caracteristicas:";
	    gotoxy(7,26);cout<<"transmision234, 7 Gbps, 4GB GDDR5";
	    gotoxy(7,27);cout<<"Stock: 9";
	    gotoxy(7,28);cout<<"Precio: S/. 1400,00";


	    gotoxy(53,10);
	    cout << "Tarjeta grafica RX 580";
	    gotoxy(55,11);cout<<"Caracteristicas:";
	    gotoxy(55,12);cout<<"transmision3004, 4 Gbps, 4GB GDDR5";
	    gotoxy(55,13);cout<<"Stock: 7";
	    gotoxy(55,14);cout<<"Precio: S/ 3200.00";

	    gotoxy(53,17);
	    cout << "Tarjeta grafica RX 570 ";
	    gotoxy(55,18);cout<<"Caracteristicas:";
	    gotoxy(55,19);cout<<"transmision4304, 10 Gbps, 4GB GDDR5";
	    gotoxy(55,20);cout<<"Stock: 12";
	    gotoxy(55,21);cout<<"Precio: S/ 3028.00";

	    gotoxy(53,24);
	    cout << "Tarjeta grafica RX VEGA";
	    gotoxy(55,25);cout<<"Caracteristicas:";
	    gotoxy(55,26);cout<<"transmision2304, 5 Gbps, 4GB GDDR5";
	    gotoxy(55,27);cout<<"Stock: 2";
	    gotoxy(55,28);cout<<"Precio: S/. 2000,00";


	    gotoxy(5,31);
	    cout << "Regresar";

	    gotoxy(5,33);
	    cout << "Salir";

	    gotoxy(3,Cursor_Pos);
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	    cout << (char)Cursor;
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	    gotoxy(1,30);

	    do{
	        if (kbhit())
	        {
	            KEY_PAD = getch();

	            switch (KEY_PAD)
	            {
	                case KEY_UP:
	                {
	                    if (Cursor_Pos >= 33) Cursor_Pos = Cursor_Pos - 2;
	                }
	                break;

	                case KEY_DOWN:
	                {
	                    if (Cursor_Pos <= 31) Cursor_Pos = Cursor_Pos + 2;
	                }
	                break;

	                case KEY_ENTER:
	                {
	                    switch (Cursor_Pos)
	                    {
	                        case 31: return TarjetaVideo5(); break;
	                        case 33: return salir(); break;
	                    }
	                }
	                break;

	                case KEY_ESC: return salir(); break;

	            }

	            for (int i = 0; i<4 ; i+=2)
	            {
	                gotoxy(3,31+i);
	                cout << (char) 0;
	            }

	            gotoxy(3,Cursor_Pos);
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Green);
	            cout << (char)Cursor;
	            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Color_Def);
	            gotoxy(1,30);
	        }
	    }while (KEY_PAD!=27);

	    return salir();






	}


void gotoxy(int x,int y)
{
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
    dwPos.X=x;
    dwPos.Y=y;
    SetConsoleCursorPosition(hcon, dwPos);
}


